-- categories: id, name
-- book_categories: book_id, category_id
create table categories
(
    id   serial primary key not null,
    name varchar(50)        not null
);

create table authors
(
    id     serial primary key not null,
    name   varchar(100)       not null,
    gender varchar(10)        not null
);

CREATE TABLE books
(
    id          serial primary key not null,
    title       varchar(50)        not null,
    import_date timestamp          not null,
    author_id   int references authors (id)
);

CREATE table book_categories
(
    book_id     int references books (id),
    category_id int references categories (id),
    primary key (book_id, category_id)
);
