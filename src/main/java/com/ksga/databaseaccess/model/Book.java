package com.ksga.databaseaccess.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Book {
    private Integer id;
    private String title;
    private LocalDateTime importDate;
    private Author author;
    private List<Category> categories;
}
