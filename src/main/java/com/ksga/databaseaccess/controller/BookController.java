package com.ksga.databaseaccess.controller;

import com.ksga.databaseaccess.model.Book;
import com.ksga.databaseaccess.model.BookRequest;
import com.ksga.databaseaccess.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/")
    public String index(Model model) {
        // select * from books;
        List<Book> books = bookService.getAllBooks();
        System.out.println(books);
        model.addAttribute("bookList", books);
        return "index";
    }

    @GetMapping("/new")
    public String getInsertForm(Model model) {
        model.addAttribute("book", new BookRequest());
        return "pages/book-form";
    }

    @GetMapping("/{id}")
    public String viewBook(@PathVariable Integer id, Model model){
        Book book = bookService.findBookById(id);
        model.addAttribute("book", book);
        return "pages/book-view";
    }

    @PostMapping("/insert")
    public String insertNewBook(
            @ModelAttribute BookRequest book
    ){
        bookService.addNewBook(book);
        return "redirect:/books/";
    }

    @GetMapping("/edit/{id}")
    public String getUpdateForm(
            @PathVariable Integer id,
            Model model
    ){
        Book book = bookService.findBookById(id);
        model.addAttribute("book", book);
        return "pages/book-edit";
    }

    @PostMapping("/update/{id}")
    public String updateBookDetails(
            @ModelAttribute Book book
    ){
        bookService.updateBook(book);
        return "redirect:/books/";
    }
    @PostMapping("/delete/{id}")
    public String deleteBookDetails(@PathVariable Integer id){
        bookService.deleteBook(id);
        return "redirect:/books/";
    }


}
