package com.ksga.databaseaccess.repository;

import com.ksga.databaseaccess.model.Author;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AuthorRepository {

    // select * from authors
    // update authors
    // delete

    @Select("SELECT * FROM authors WHERE id = #{authorId}")
    Author getAuthorById(Integer authorId);
}
