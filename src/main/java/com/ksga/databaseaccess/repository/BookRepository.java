package com.ksga.databaseaccess.repository;

import com.ksga.databaseaccess.model.Author;
import com.ksga.databaseaccess.model.Book;
import com.ksga.databaseaccess.model.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {

    @Select("SELECT id, id as book_id, title, import_date, author_id FROM books")
    @Result(property = "importDate", column = "import_date")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.ksga.databaseaccess.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories", column = "book_id",
            many = @Many(select = "com.ksga.databaseaccess.repository.CategoryRepository.getCategoriesOfBookById"))
    List<Book> getAllBooks();

    @Select("SELECT * FROM books " +
            "WHERE id = #{bookId}")
    @Result(property = "importDate", column = "import_date")
    @Result(property = "author", column = "author_id",
            one = @One(select = "com.ksga.databaseaccess.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories", column = "book_id",
            many = @Many(select = "com.ksga.databaseaccess.repository.CategoryRepository.getCategoriesOfBookById"))
    Book findById(Integer bookId);

    @Insert("INSERT INTO books (title, import_date, author_id) " +
            "VALUES (#{book.title}, #{book.importDate}, #{book.authorId})")
    void insert(@Param("book") BookRequest book);

    @Update("UPDATE books " +
            "SET title = #{book.title} " +
            "WHERE id = #{book.id}")
    void updateBook(@Param("book") Book book);

    @Delete("DELETE FROM books WHERE id = #{bookId}")
    void remove(Integer bookId);
}
