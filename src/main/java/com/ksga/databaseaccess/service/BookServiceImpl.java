package com.ksga.databaseaccess.service;

import com.ksga.databaseaccess.model.Book;
import com.ksga.databaseaccess.model.BookRequest;
import com.ksga.databaseaccess.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.getAllBooks();
    }

    @Override
    public Book findBookById(Integer bookId) {
        return bookRepository.findById(bookId);
    }

    @Override
    public void addNewBook(BookRequest book) {
        book.setImportDate(LocalDateTime.now());
        bookRepository.insert(book);
    }

    @Override
    public void updateBook(Book book) {
        bookRepository.updateBook(book);
    }

    @Override
    public void deleteBook(Integer bookId) {
        bookRepository.remove(bookId);
    }
}
