package com.ksga.databaseaccess.service;

import com.ksga.databaseaccess.model.Book;
import com.ksga.databaseaccess.model.BookRequest;

import java.util.List;

public interface BookService {

    List<Book> getAllBooks();

    Book findBookById(Integer bookId);

    void addNewBook(BookRequest book);

    void updateBook(Book book);

    void deleteBook(Integer bookId);
}
